class SessionrecChannel < ApplicationCable::Channel
  def subscribed
    @sessionrec= Sessionrec.find(params[:id])
    stream_for @sessionrec
  end

  def received(data)
    SessionrecChannel.broadcast_to(@sessionrec, {sessionrec: @sessionrec, data: @sessionrec.data })
  end

  def unsubscribed
    #handle dissconnect.....proably by upload

    # Any cleanup needed when channel is unsubscribed
  end
end
