class Sessionrec < ApplicationRecord
  belongs_to :session

  before_save :setjoin

  def setjoin
    joinc= SecureRandom.hex(10)[0,5]
    self.joincode= joinc
    self.joinqr= "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=#{joinc}"
  end

end
