class Session < ApplicationRecord
  has_many :tasks
  belongs_to :user
  has_one :sessionrec

  before_save :sesrec_create

  def sesrec_create
    #create sessionrec at the same time with session?
    Sessionrec.create(session_id: self.id) #do I have an id?

  end
end
