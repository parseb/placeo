class User < ApplicationRecord
#  require 'digest' #move
  has_many :sessions
  #validates_uniqueness_of :email
  #has_secure_password
  before_create :setauth


private
  def setauth
    self.auth= SecureRandom.hex(8)[0,8]
  end

end
