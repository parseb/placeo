Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  scope '/api' do
    resources :users
    resources :tasks
    resources :sessions
    resources :sessionrecs

    get 'getuser', to: 'users#getuser'
    get 'startss', to: 'sessionrecs#starts'
    get 'joins', to: 'sessionrecs#joins'
  end

  mount ActionCable.server, at: "/cable"

end
