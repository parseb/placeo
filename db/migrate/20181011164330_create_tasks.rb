class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :body
      t.references :session, foreign_key: true
      t.string :link

      t.timestamps
    end
  end
end
