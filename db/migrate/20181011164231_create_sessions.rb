class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string :name
      t.datetime :time
      #t.references :tasks, foreign_key: true
      t.references :user, foreign_key: true
      t.text :details
      t.string :invite
      t.string :state

      t.timestamps
    end
  end
end
