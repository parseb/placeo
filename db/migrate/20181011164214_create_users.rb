class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :auth
      t.string :phone
      t.text :bio
      t.string :email
      t.string :avatar
      t.string :token
      #t.string :password_digest
      #t.string :username

      t.timestamps
    end
  end
end
