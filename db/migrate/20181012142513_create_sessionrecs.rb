class CreateSessionrecs < ActiveRecord::Migration[5.2]
  def change
    create_table :sessionrecs do |t|
      t.string :joincode
      t.references :session, foreign_key: true
      t.text :transcript
      t.text :data

      t.timestamps
    end
  end
end
